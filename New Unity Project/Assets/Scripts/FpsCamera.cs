﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FpsCamera : MonoBehaviour {

	public Camera FPSCamera;
	public float horizontal_Speed;
	public float vertical_Speed;

	public float Limit_angle_Top;
	public float Limit_angle_Bot;

	float h;
	float v;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		h = horizontal_Speed * Input.GetAxis ("Mouse X") * Time.deltaTime;
		v = vertical_Speed * Input.GetAxis ("Mouse Y") * Time.deltaTime;

		transform.Rotate (0, h, 0);
		FPSCamera.transform.Rotate (-v, 0, 0);
	}
}
