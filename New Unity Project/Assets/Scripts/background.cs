﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class background : MonoBehaviour 
{
	private float Tam_Plantilla = 10.0f;
	private int Height = 20;
	private int Width = 20;
	public List<GameObject> Plantillas = new List<GameObject>();

	// Use this for initialization
	void Start () 
	{
		for (int j = 0; j < Width; j++) 
		{
			for (int k = 0; k < Height; k++) 
			{
					Instantiate (Plantillas[Random.Range(0,Plantillas.Count)], new Vector3(k*Tam_Plantilla,0,j*Tam_Plantilla), transform.rotation);
			}
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
