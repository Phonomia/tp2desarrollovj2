﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Credits : MonoBehaviour 
{

	public void Button_ToMenu()
	{
		SceneManager.LoadScene ("Menu");
	}

	public void Button_quit()
	{
		Application.Quit ();
	}

}
