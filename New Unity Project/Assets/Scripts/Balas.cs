﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balas : MonoBehaviour {

	public int force;
	private Vector3 initial_pos;

	public GameObject puntos;
	private Rigidbody rb;
	// Use this for initialization
	void Start () 
	{
		initial_pos = transform.position;
		rb = GetComponent<Rigidbody> ();
		rb.AddForce (transform.forward * force);
	}
	// Update is called once per frame

	void OnCollisionEnter(Collision otherobj)
	{
		if (otherobj.gameObject.tag == "Trampa" ) 
		{
			//otherobj.gameObject.SendMessage ("sumar");
			otherobj.gameObject.SetActive (false);
			//player.SendMessage ("puntaje");
			Destroy (gameObject);
		}
		if (otherobj.gameObject.tag == "Paredes") 
		{
			Destroy (gameObject);
		}
	
	}

	void OnTriggerExit (Collider other)
	{
		if (other.gameObject.tag == "sphere") 
		{
			Destroy (gameObject);
		}
	}
		
}
