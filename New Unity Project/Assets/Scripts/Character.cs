﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Character : MonoBehaviour 
{

	private Animator Character_Animator;
	public int Vida = 100;
	private Rigidbody rb;
	public float speed;
	public float Retroceso;

	// Use this for initialization
	void Start () 
	{
		Character_Animator = this.gameObject.GetComponent<Animator>();
		rb = GetComponent<Rigidbody> ();
	}

	void Move()
	{
		float translation_x = Input.GetAxis ("Vertical") * speed;
		translation_x *= Time.deltaTime;
		float translation_z = Input.GetAxis ("Horizontal") * speed;
		translation_z *= Time.deltaTime;
		transform.Translate (0, 0, translation_x);
		transform.Translate (translation_z, 0, 0);
		walking ();

	}
		
	void OnCollisionEnter(Collision otherobj)
	{
		if (otherobj.gameObject.tag == "Trampa") 
		{
			Vida -= 50;
			Vector3 dir = otherobj.contacts[0].point - transform.position;
			dir = -dir.normalized;
			rb.AddForce (dir * Retroceso);
		}
	}
	void walking()
	{
		if (Input.GetAxis ("Vertical") != 0) 
			Character_Animator.SetBool ("Walking", true);
		else
			Character_Animator.SetBool ("Walking", false);
	}

	// Update is called once per frame
	void Update () 
	{
		Move ();
		if (Vida <= 0)
			SceneManager.LoadScene ("Creditos");
	}
}
